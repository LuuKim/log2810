#ifndef ETAT_H
#define ETAT_H

#include <string>
#include <vector>


using namespace std;

class Etat
{
public :
	Etat();
	Etat(string etat, bool initial, bool final_);
   void setEtat(string etat);
    void setInitial(bool initial);
    void setFinal(bool final);
    string getEtat();
    bool getInitial();
    bool getFinal();
    Etat* getunEtat(string characters);
    Etat* AjouterEtat(string characters);


private :
	string etat_;
	bool initial_;
	bool final_;
    vector<Etat*> etats_;
    
};

#endif
