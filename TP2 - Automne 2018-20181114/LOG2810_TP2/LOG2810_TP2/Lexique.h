
#ifndef LEXIQUE_H
#define LEXIQUE_H

#include <string>
#include <vector>
#include "Etat.h"


using namespace std;

class Lexique : public Etat
{
    public :
    bool AjouterEtat(string etat);
    vector<string> Liste(string motTape, int NombreChoix);
    bool Existe(string motTape);
    void LectureFichier(string nomFichier);
    vector<string> Comparer(string DebutMot, int NombreMots);
    
    private :
    vector<string> AfficherMots_;
    Etat* etat0 ;
    string nomFichier_;
};

#endif

