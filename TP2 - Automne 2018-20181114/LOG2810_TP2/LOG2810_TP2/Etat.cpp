#include "Etat.h"

Etat::Etat()
{
	etat_ = "";
	initial_ = false;
	final_ = false;
}

Etat::Etat(string etat, bool initial, bool final) 
	: etat_(etat), initial_(initial), final_(final) {}

void Etat::setEtat(string etat)
{
	etat_ = etat;
}

void Etat::setInitial(bool initial)
{
	initial_ = initial;
}

void Etat::setFinal(bool final)
{
	final_ = final;
}

string Etat::getEtat()
{
	return etat_;
}

bool Etat::getInitial()
{
	return initial_;
}

bool Etat::getFinal()
{
	return final_;
}

Etat* Etat::getunEtat(string characters) //child
{
    Etat* unEtat = nullptr ;
   // for(vector<Etat*>::iterator it = etats_.begin(); it!=etats_.end(); it++)
    for(int i = 0 ; i < etats_.size(); i++)
    {
        if (etats_[i]->getEtat() == characters)
        {
            unEtat = etats_[i] ;
        break;
    }

}
    
    return unEtat;
}


Etat* Etat::AjouterEtat(string characters)
{
    Etat* AjoutEtat  = getunEtat(characters);
    
    if(AjoutEtat == nullptr)
    {
        AjoutEtat = new Etat();
    }
    etats_.push_back(AjoutEtat);
    
    return AjoutEtat;
}
























