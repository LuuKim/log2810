public class EtatFinal {
    private String nom;
    private Integer nbFoisUtilisee;
    private Integer estRecemmentUtilisee;

    public EtatFinal(String n) {
        nom = n;
        nbFoisUtilisee = 0;
        estRecemmentUtilisee = 0;
    }

    public void setNom(String n)
    {
        nom = n;
    }
    public String getNom() {
        return nom;
    }
    public void setNbFoisUtilisee(Integer nb){
        nbFoisUtilisee = nb;
    }
    public Integer getNbFoisUtilisee(){
        return nbFoisUtilisee;
    }

    public void setEstRecemmentUtilisee(Integer rec) {
        estRecemmentUtilisee = rec;
    }

    public Integer getEstRecemmentUtilisee() {
        return estRecemmentUtilisee;
    }

    public void afficherLabels(){
        String temp ="";
        temp += "Les labels pour le mot ";
        temp += nom;
        temp += " sont:\n";
        temp += "\tNombre de fois utilis�: ";
        temp +=  Integer.toString(nbFoisUtilisee) + "\n";
        temp += "\tFait partie des cinq mots r�cemment utilis�s: ";
        temp += Integer.toString(estRecemmentUtilisee) + "\n";
        System.out.println(temp);
    }

}
