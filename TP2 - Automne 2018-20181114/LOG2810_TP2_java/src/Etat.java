
public class Etat {
    private String nom;
    private boolean etatFinal;
    private Etat etatPrecedent;

    public Etat() {}

    public Etat(String n, Etat p)
    {
        nom = n;
        etatPrecedent = p;
        etatFinal = false;
    }

    public Etat(String n, Etat p, boolean f) //pour les states finaux
    {
        nom = n;
        etatPrecedent = p;
        etatFinal = f;
    }

    public void setNom(String n)
    {
        nom = n;
    }

    public void setEtatPrecedent (Etat p) { etatPrecedent = p; }

    public void setFinal(boolean f)
    {
        etatFinal = f;
    }

    public String getNom() {
        return nom;
    }

    public boolean getFinal()
    {
        return etatFinal;
    }

    public Etat getEtatPrecedent() {
        return etatPrecedent;
    }

}
