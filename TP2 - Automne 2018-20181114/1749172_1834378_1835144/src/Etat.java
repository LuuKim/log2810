
public class Etat {
    private String nom;
    private boolean etatFinal;
    private Etat etatPrecedent;
    private Integer nbFoisUtilisee = 0;
    private Integer estRecemmentUtilisee = 0;

    public Etat() {}

    public Etat(String n, Etat p)
    {
        nom = n;
        etatPrecedent = p;
        etatFinal = false;
    }

    public Etat(String n, Etat p, boolean f) //pour les states finaux
    {
        nom = n;
        etatPrecedent = p;
        etatFinal = f;
    }

    public Etat(String n, Etat p, Integer nbUtilisation)
    {
        nom = n;
        etatPrecedent = p;
        nbFoisUtilisee = nbUtilisation;
    }

    public void setEstRecemmentUtilisee(Integer n)
    {
        estRecemmentUtilisee = n;
    }

    public void setNbFoisUtilisee(Integer nb){
        nbFoisUtilisee = nb;
    }

    public void setNom(String n)
    {
        nom = n;
    }

    public void setEtatPrecedent (Etat p) { etatPrecedent = p; }

    public void setFinal(boolean f)
    {
        etatFinal = f;
    }

    public String getNom() {
        return nom;
    }

    public boolean getFinal()
    {
        return etatFinal;
    }

    public Etat getEtatPrecedent() {
        return etatPrecedent;
    }

    public Integer getNbFoisUtilisee(){
        return nbFoisUtilisee;
    }
    public Integer getEstRecemmentUtilisee() {
        return estRecemmentUtilisee;
    }

    public void afficherLabels(){
        String temp ="";
        temp += "Les labels pour le mot ";
        temp += nom;
        temp += " sont:\n";
        temp += "\tNombre de fois utilise: ";
        temp +=  Integer.toString(nbFoisUtilisee) + "\n";
        temp += "\tFait partie des cinq mots recemment utilises: ";
        temp += Integer.toString(estRecemmentUtilisee) + "\n";
        System.out.println(temp);
    }
}

