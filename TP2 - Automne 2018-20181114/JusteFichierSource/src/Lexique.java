import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.Queue;


public class Lexique {

    private List<Etat> mots = new ArrayList();
    private List<Etat> motsFinaux = new ArrayList();
    private Queue<Integer> fifo = new LinkedList<>();
    private Etat initial = new Etat();


    public Lexique() {}

    // Fonctions pour lireFichier
    public String toString(char c)
    {
        return Character.toString(c);
    }

    public List<Etat> getMotsFinaux() {
        return motsFinaux;
    }

    public boolean contient(Etat a, Etat b) {
        return (a.getNom().equals(b.getNom()));
    }

    public void ajouter(Etat a) {

        for(Etat i : mots)
        {
            if(contient(a,i)) {
                return;
            }
        }
        mots.add(a);
    }

    public void lireFichier(String filePath)
    {
        try {
            initial.setEtatPrecedent(initial);
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String lu;

            while ((lu = in.readLine()) != null) {

                List<Etat> temp = new ArrayList();

                for (int i = 0; i < lu.length(); i++) {

                    if (mots.isEmpty()) {
                        String nom = toString(lu.charAt(i));
                        Etat e = new Etat(nom, initial);
                        temp.add(e);
                        ajouter(e);
                    }
                    else {
                        if (i == 0) {
                            String nom = toString(lu.charAt(i));
                            Etat e = new Etat(nom, initial);
                            temp.add(e);
                            ajouter(e);
                        } else {
                            String nom = temp.get(i - 1).getNom() + toString(lu.charAt(i));
                            Etat precedent = temp.get(i - 1);
                            Etat e = new Etat(nom, precedent);

                            if (i != 0 && i != lu.length() - 1) {
                                temp.add(e);
                                ajouter(e);

                            } else if (i == lu.length() - 1) {
                                e.setFinal(true);
                                temp.add(e);
                                motsFinaux.add(e);
                                ajouter(e);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void afficher()
    {
        for(int i = 0; i < mots.size(); i++)
        {
            System.out.println(mots.get(i).getNom());
            if (mots.get(i).getFinal())
                System.out.println('\n');
        }

    }

    // Fonction pour afficher
    public int min(String a, String b)
    {
        if(a.length() >= b.length())
        {
            return b.length();
        }
        else if(b.length() > a.length())
        {
            return a.length();
        }
        return 0;
    }

    public boolean memeDebutMot(String a, String b)
    {
        int compteur = 0;
        for(int i = 0; i < min(a,b); i++)
        {
            if(a.charAt(i) == b.charAt(i))
            {
                compteur += 1;
            }
        }
        return compteur == min(a,b);
    }

    public List<String> afficher(String s)
    {
        List<String> temp = new ArrayList();
        for(int i = 0; i < motsFinaux.size(); i++)
        {
            if(motsFinaux.get(i).getNom().length() >= s.length()) {
                if(memeDebutMot(s,motsFinaux.get(i).getNom()))
                {
                    temp.add(motsFinaux.get(i).getNom());
                }
            }
        }
        return temp;
    }

    public void miseajourLabel(String mot)
    {
        // Si mot est un mot final,
        Boolean isContained = false;
        Integer index = -1;
        for (ListIterator<Etat> iter = motsFinaux.listIterator(); iter.hasNext(); ) {
            Etat eFinal = iter.next();
            if(eFinal.getNom().equals(mot)&& eFinal.getFinal() == true){
                isContained = true;
                index = iter.previousIndex();
            }
        }
        if(isContained){
            motsFinaux.get(index).setNbFoisUtilisee(motsFinaux.get(index).getNbFoisUtilisee()+1);
            if(fifo.size()==5){
                motsFinaux.get(fifo.peek()).setEstRecemmentUtilisee(0);
                fifo.remove();
            }
            motsFinaux.get(index).setEstRecemmentUtilisee(1);
            fifo.add(index);
        }
    }

    public void viderLexique(){
        motsFinaux.clear();
    }

    public String afficherLabels(){
        String temp ="";
        temp += "Labels pour le lexique:\n";
        temp += String.format("%-20s%-10s%-10s\n","Mot du lexique","Label 1","Label 2");
        for (Etat mot : motsFinaux) {
            temp += String.format("%-20s%-10s%-10s\n",mot.getNom(),
                    Integer.toString(mot.getNbFoisUtilisee()),
                    Integer.toString(mot.getEstRecemmentUtilisee()));

        }
        return temp;
    }
}
