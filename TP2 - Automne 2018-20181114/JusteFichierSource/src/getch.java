import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JTextField;


public class getch {

    static Lexique lexique = new Lexique();

    public static void getCh() {
        List<String> listMot = new ArrayList();

        final JFrame frame = new JFrame();
        frame.setTitle("Menu du Lexique");
        frame.setSize(400, 300);
        frame.setLocationRelativeTo(null);
        JPanel panel = new JPanel();
        panel.setBackground(Color.YELLOW);
        frame.setContentPane(panel);
        JLabel label1 = new JLabel();
        label1.setText("Choisir le lexique a lire:");
        label1.setBounds(10, 10, 100, 100);
        frame.add(label1);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JButton b1 , b2, b3, b4, b5, b6;
        b1 = new JButton("1");
        b2 = new JButton("2");
        b3 = new JButton("3");
        b4 = new JButton("4");
        b5 = new JButton("5");
        b6 = new JButton("6");
        frame.add(b1);
        frame.add(b2);
        frame.add(b3);
        frame.add(b4);
        frame.add(b5);
        frame.add(b6);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique 1.txt");

            }});
        b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique 2.txt");
            }});
        b3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique 3.txt");
            }});
        b4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique 4.txt");
            }});
        b5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique5.txt");
            }});
        b6.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                lexique.viderLexique();
                lexique.lireFichier("./src/lexique6.txt");
            }});
        JButton button;
        JButton button1;
        JButton button2;
        button = new JButton("Chercher dans le lexique");
        button1 = new JButton("Afficher les deux labels");
        button2 = new JButton("Quitter le systeme");
        frame.add(button);
        frame.add(button1);
        frame.add(button2);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                final JFrame frame1 = new JFrame();
                frame1.setTitle("Chercher dans le lexique: ");
                frame1.setSize(400, 400);
                frame1.setLocationRelativeTo(null);
                JPanel panel1 = new JPanel();
                panel1.setBackground(Color.ORANGE);
                frame1.setContentPane(panel1);
                JLabel label = new JLabel();
                panel1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                label.setText("Entrer le mot recherche :");
                label.setBounds(10, 10, 100, 100);
                frame1.add(label);
                JTextField textfield = new JTextField();
                textfield.setPreferredSize(new Dimension(200, 24));
                frame1.add(textfield);

                JTextArea displayArea = new JTextArea();
                displayArea.setEditable(false);
                JScrollPane scrollPane = new JScrollPane(displayArea);
                scrollPane.setPreferredSize(new Dimension(375, 250));
                frame1.add(scrollPane);

                JButton button4 , button5 , b00;
                button4 = new JButton("Retourner au menu");
                button5 = new JButton("Quitter le systeme");
                frame1.add(button4);
                frame1.add(button5);
                frame1.setLayout(new FlowLayout());
                frame1.setVisible(true);
                button4.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        String lastWord = textfield.getText().replaceAll("^.*?(\\w+)\\W*$", "$1");
                        listMot.add(lastWord);
                        for(String mot : listMot){
                            lexique.miseajourLabel(mot);
                        }
                        listMot.clear();
                        frame1.dispose();
                    }
                });
                button5.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Vous avez choisi de quitter le systeme ");
                        frame.dispose();
                        frame1.dispose();
                    }
                });
                textfield.addKeyListener(new java.awt.event.KeyAdapter() {
                    String mot = "";
                    String motFinal = "";

                    public void keyPressed(KeyEvent e){

                    }
                    public void keyTyped(KeyEvent e) {

                        // Invoked when a key has been released.
                        char ch = e.getKeyChar();
                        if(ch == KeyEvent.VK_BACK_SPACE) {
                            if (mot != null && mot.length() > 0 ) {
                                mot = mot.substring(0, mot.length() - 1);
                            }
                            else{
                                // Si on arrive sur des mots avec back_space
                                // textfield.getText()
                            }
                        }
                        else if(ch == KeyEvent.VK_PERIOD || ch == KeyEvent.VK_COMMA || ch == KeyEvent.VK_SPACE ){
                            motFinal = mot;
                            mot = "";
                            displayArea.setText(mot);
                        }
                        else {
                            mot += ch;
                        }

                        if(mot != ""){
                            List<String> temp = new ArrayList();
                            temp = lexique.afficher(mot);
                            String text = "";
                            for(String t : temp){
                                text += t + "\n";
                            }
                            displayArea.setText(text);
                        }
                        else{
                            // S'assurer qu'un mot final n'est process qu'une seule fois.
                            if(motFinal != "") {
                                listMot.add(motFinal);
                                motFinal = "";
                            }
                        }
                    }

                    public void keyReleased(KeyEvent e) {

                    }

                });
            }
        });

		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
                final JFrame frame2 = new JFrame();
                frame2.setTitle("Afficher les deux labels: ");
                frame2.setSize(400, 400);
                frame2.setLocationRelativeTo(null);

                JPanel panel2 = new JPanel();
                panel2.setBackground(Color.BLUE);
                frame2.setContentPane(panel2);
                panel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

                JTextArea displayArea = new JTextArea();
                displayArea.setEditable(false);
                JScrollPane scrollPane = new JScrollPane(displayArea);
                scrollPane.setPreferredSize(new Dimension(375, 250));
                frame2.add(scrollPane);

                displayArea.setText(lexique.afficherLabels());

                JButton button6 , button7;
                button6 = new JButton("Retourner au menu");
                button7 = new JButton("Quitter le systeme");
                frame2.add(button6);
                frame2.add(button7);
                frame2.setLayout(new FlowLayout());
                frame2.setVisible(true);

                button6.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        frame2.dispose();
                    }
                });
                button7.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Vous avez choisi de quitter le systeme ");
                        frame.dispose();
                        frame2.dispose();
                    }
                });

			}
    });
        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("Vous avez choisi de quitter le systeme ");
                frame.dispose();
            }
        });
    }
}
