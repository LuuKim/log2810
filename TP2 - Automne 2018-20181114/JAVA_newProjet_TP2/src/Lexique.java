import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;

public class Lexique {

    private List<Etat> mots = new ArrayList();
    private List<Etat> motsFinaux = new ArrayList();
    private Etat initial = new Etat();

    public Lexique() {}

    // Fonctions pour lireFichier
    public String toString(char c)
    {
        return Character.toString(c);
    }

    public List<Etat> getMotsFinaux() {
        return motsFinaux;
    }

    public boolean contient(Etat a, Etat b) {
        return (a.getNom().equals(b.getNom()));
    }

    public void ajouter(Etat a) {

        for(Etat i : mots)
        {
            if(contient(a,i)) {
                return;
            }
        }
        mots.add(a);
    }

    public void lireFichier(String filePath)
    {
        try {
            initial.setEtatPrecedent(initial);
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            String lu;

            while ((lu = in.readLine()) != null) {

                List<Etat> temp = new ArrayList();

                for (int i = 0; i < lu.length(); i++) {

                    if (mots.isEmpty()) {
                        String nom = toString(lu.charAt(i));
                        Etat e = new Etat(nom, initial);
                        temp.add(e);
                        ajouter(e);
                    }
                    else {
                        if (i == 0) {
                            String nom = toString(lu.charAt(i));
                            Etat e = new Etat(nom, initial);
                            temp.add(e);
                            ajouter(e);
                        } else {
                            String nom = temp.get(i - 1).getNom() + toString(lu.charAt(i));
                            Etat precedent = temp.get(i - 1);
                            Etat e = new Etat(nom, precedent);

                            if (i != 0 && i != lu.length() - 1) {
                                temp.add(e);
                                ajouter(e);

                            } else if (i == lu.length() - 1) {
                                e.setFinal(true);
                                temp.add(e);
                                motsFinaux.add(e);
                                ajouter(e);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // Inutile à première vue
    public void afficher()
    {
        for(int i = 0; i < mots.size(); i++)
        {
            System.out.println(mots.get(i).getNom());
            if (mots.get(i).getFinal())
                System.out.println('\n');
        }

    }

    // Fonction pour afficher
    public int min(String a, String b)
    {
        if(a.length() >= b.length())
        {
            return b.length();
        }
        else if(b.length() > a.length())
        {
            return a.length();
        }
        return 0;
    }

    public boolean memeDebutMot(String a, String b)
    {
        int compteur = 0;
        for(int i = 0; i < min(a,b); i++)
        {
            if(a.charAt(i) == b.charAt(i))
            {
                compteur += 1;
            }
        }
        return compteur == min(a,b);
    }

    public void afficher(String s)
    {
        List<String> temp = new ArrayList();
        for(int i = 0; i < motsFinaux.size(); i++)
        {
            if(motsFinaux.get(i).getNom().length() >= s.length()) {
                if(memeDebutMot(s,motsFinaux.get(i).getNom()))
                {
                    temp.add(motsFinaux.get(i).getNom());
                }
            }
        }
        for(int j = 0; j < temp.size(); j++) {
            System.out.println(temp.get(j));
        }
    }

    public ArrayList DisplayLexique(String uneEntree)
    {
        ArrayList<String> afficherListe = new ArrayList();
        int taille = uneEntree.length()  ;
        for (int i = 0 ; i < motsFinaux.size() ; i++)
        {
            for(int j = 0 ; j < motsFinaux.get(i).getNom().substring(0,taille ).length() ; j++) {
                if ((motsFinaux.get(i).getNom().substring(0,taille).equals(uneEntree)))
                {
                    afficherListe.add(motsFinaux.get(i).getNom());
                    break;
                }

            }

        }
      //  for(int i = 0 ; i< afficherListe.size(); i++) {
        //System.out.println(afficherListe.get(i));
        return afficherListe;
    }

    public void viderLexique(){
        motsFinaux.clear();
    }

    public void motsPlusUtilises()
    {
        Integer mot1 = 0;
        Integer mot2 = 0;
        Integer mot3 = 0;
        Integer mot4 = 0;
        Integer mot5 = 0;
        for(int i = 0; i < motsFinaux.size(); i++)
        {
            Integer e = motsFinaux.get(i).getNbFoisUtilisee();
            if(e >= mot1 &&         //Premier mot le plus utilisé
                e > mot2 &&
                e > mot3 &&
                e > mot4 &&
                e > mot5)
            {
                mot1 = e;
                motsFinaux.get(i).setEstRecemmentUtilisee(1);
            }
            else if(e >= mot2 &&    //Deuxième mot le plus utilisé
                    e > mot3 &&
                    e > mot4 &&
                    e > mot5 &&
                    e < mot1)
            {
                mot2 = e;
                motsFinaux.get(i).setEstRecemmentUtilisee(1);
            }
            else if(e < mot2 &&    //Troisième mot le plus utilisé
                    e >= mot3 &&
                    e > mot4 &&
                    e > mot5 &&
                    e < mot1)
            {
                mot3 = e;
                motsFinaux.get(i).setEstRecemmentUtilisee(1);
            }
            else if(e < mot2 &&    //Quatrième mot le plus utilisé
                    e < mot3 &&
                    e >= mot4 &&
                    e > mot5 &&
                    e < mot1)
            {
                mot4 = e;
                motsFinaux.get(i).setEstRecemmentUtilisee(1);
            }
            else if(e < mot2 &&    //Cinquième mot le plus utilisé
                    e < mot3 &&
                    e < mot4 &&
                    e >= mot5 &&
                    e < mot1)
            {
                mot5 = e;
                motsFinaux.get(i).setEstRecemmentUtilisee(1);
            }
            else
            {
                motsFinaux.get(i).setEstRecemmentUtilisee(0);
            }
        }
    }
}